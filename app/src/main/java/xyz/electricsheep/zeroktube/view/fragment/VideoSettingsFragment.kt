package xyz.electricsheep.zeroktube.view.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest
import java.util.*


class VideoSettingsFragment : BottomSheetDialogFragment() {
    private lateinit var ctx: Context

    private lateinit var menu: LinearLayout
    private lateinit var iconQuality: ImageView
    private lateinit var textQuality: TextView
    private lateinit var iconSpeed: ImageView
    private lateinit var textSpeed: TextView
    private lateinit var quality: LinearLayout
    private lateinit var speed: LinearLayout

    private lateinit var video: VideoIdRequest

    fun newInstance(video: VideoIdRequest): VideoSettingsFragment {
        this.video = video
        return this
    }

    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        quality = inflater.inflate(R.layout.row_popup_menu, null) as LinearLayout
        speed = inflater.inflate(R.layout.row_popup_menu, null) as LinearLayout

        return inflater.inflate(R.layout.fragment_video_settings_popup_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        menu = view.findViewById(R.id.video_settings_popup)

        iconQuality = quality.findViewById(R.id.icon)
        iconQuality.setImageResource(R.drawable.ic_high_quality_white)
        textQuality = quality.findViewById(R.id.text)
        textQuality.setText(R.string.quality)
        menu.addView(quality)

        iconSpeed = speed.findViewById(R.id.icon)
        iconSpeed.setImageResource(R.drawable.ic_slow_motion_video_white)
        textSpeed = speed.findViewById(R.id.text)
        textSpeed.setText(R.string.speed)
        menu.addView(speed)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        if (context != null) {
            ctx = context!!
        }

        // When user click on the qualities options
        quality.setOnClickListener {
            val videoQualitiesFragment: VideoQualitiesFragment = VideoQualitiesFragment().newInstance(video)
            videoQualitiesFragment.show(activity!!.supportFragmentManager,"VideoQualities")
        }

        // When user click on the speeds options
        speed.setOnClickListener {
            val text = "This option is not yet implemented"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(ctx, text, duration)
            toast.show()
        }

        super.onActivityCreated(savedInstanceState)
    }
}