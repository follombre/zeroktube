package xyz.electricsheep.zeroktube.view.fragment

import android.content.Context
import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest
import xyz.electricsheep.zeroktube.request.video.ZerokDatabase
import java.io.IOException
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


class VideoPlayerFragment : Fragment(), SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, CoroutineScope by MainScope() {
    private lateinit var ctx: Context

    private lateinit var surfaceView: SurfaceView
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var surfaceHolder: SurfaceHolder

    private lateinit var videoPlayer: ConstraintLayout
    private lateinit var videoControl: ConstraintLayout

    private lateinit var textMaxTime: TextView
    private lateinit var textCurrentPosition: TextView
    private lateinit var seekBar: SeekBar
    private val threadHandler = Handler()
    private var duration: Int = 0
    private var currentPosition: Int = 0
    private var isPlaying: Boolean = true

    private var videoControlVisibility: Boolean = true

    private lateinit var settings: ImageView

    private lateinit var doRewind: ImageView
    private lateinit var doPlayPause: ImageView
    private lateinit var doFastForward: ImageView

    private lateinit var fullScreen: ImageView

    private var videoId: Int = 0
    private lateinit var zerokDatabase: ZerokDatabase
    private lateinit var video: VideoIdRequest

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(BuildConfig.DEBUG) {
            Log.d("VideoPlayerFragmentGaet", "onCreateView")
        }
        return inflater.inflate(R.layout.fragment_video_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(BuildConfig.DEBUG) {
            Log.d("VideoPlayerFragmentGaet", "onViewCreated")
        }
        videoPlayer = view.findViewById(R.id.videoPlayer)
        videoControl = view.findViewById<View>(R.id.videoControl) as ConstraintLayout

        surfaceView = view.findViewById(R.id.surfaceView)

        textCurrentPosition = view.findViewById(R.id.textView_currentPosion)
        textMaxTime = view.findViewById(R.id.textView_maxTime)

        seekBar = view.findViewById(R.id.seekBar)

        settings = view.findViewById(R.id.button_settings)

        doRewind = view.findViewById(R.id.button_rewind)
        doPlayPause = view.findViewById(R.id.playVideo)
        doFastForward = view.findViewById(R.id.button_fastForward)

        fullScreen = view.findViewById(R.id.fullScreen)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        if(BuildConfig.DEBUG) {
            Log.d("VideoPlayerFragmentGaet", "onActivityCreated")
        }
        if (context != null) {
            ctx = context!!
        }
        zerokDatabase = ZerokDatabase.getInstance(ctx)
        runBlocking {
            launch {

                video = zerokDatabase.videoIdDao().getVideoIdRequest(videoId)
                video.files = zerokDatabase.filesDao().getAllVideoFiles(videoId)

            }
        }
        surfaceHolder = surfaceView.holder
        surfaceHolder.addCallback(this)

        seekBar.isClickable = true

        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt("currentPosition")
            isPlaying = savedInstanceState.getBoolean("isPlaying")
        }

        // When user click on the Video Player
        videoPlayer.setOnClickListener {
            setVisibility()
        }

        // When user click to "Settings"
        settings.setOnClickListener {
            val videoSettingsFragment: VideoSettingsFragment = VideoSettingsFragment().newInstance(video)
            videoSettingsFragment.show(activity!!.supportFragmentManager,"VideoSettings")
        }

        // When user click to "Rewind"
        doRewind.setOnClickListener {
            duration = this.mediaPlayer.duration
            // 5 seconds.
            val SUBTRACT_TIME = 5000

            if (currentPosition - SUBTRACT_TIME > 0) {
                this.mediaPlayer.seekTo(currentPosition - SUBTRACT_TIME)
            }
        }

        // When user click to "Play" or "Pause"
        doPlayPause.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                pause()
                doPlayPause.setImageResource(R.drawable.ic_play_white)
            } else {
                doPlayPause.setImageResource(R.drawable.ic_pause_white)
                start()
            }
        }

        // When user click to "FastForward"
        doFastForward.setOnClickListener {
            duration = this.mediaPlayer.duration
            // 5 seconds.
            val ADD_TIME = 5000

            if (currentPosition + ADD_TIME < duration) {
                this.mediaPlayer.seekTo(currentPosition + ADD_TIME)
            }
        }

        super.onActivityCreated(savedInstanceState)
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        if(BuildConfig.DEBUG) {
            Log.d("VideoPlayerFragmentGaet", "surfaceCreated")
        }
        mediaPlayer = MediaPlayer()
        mediaPlayer.setDisplay(holder)

        try {
            mediaPlayer.setDataSource(video.files[0].fileUrl)
            mediaPlayer.prepare()
            mediaPlayer.setOnPreparedListener(this)
            // The duration in milliseconds
            duration = this.mediaPlayer.duration
            this.seekBar.max = duration
            val maxTimeString = this.millisecondsToString(duration)
            this.textMaxTime.text = maxTimeString
            mediaPlayer.seekTo(currentPosition)
            if (isPlaying) {
                mediaPlayer.start()
                start()
            } else {
                pause()
            }
//            mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        setVideoSize()
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
    }

    override fun onPrepared(mp: MediaPlayer?) {
    }

    fun setVideoId(id: Int) {
        videoId = id
    }

    private fun setVideoSize() {
        // Get the dimensions of the video
        val videoWidth: Int = mediaPlayer.videoWidth
        val videoHeight: Int = mediaPlayer.videoHeight
        val videoProportion: Float = videoWidth.toFloat() / videoHeight.toFloat()

        // Get the width of the screen
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val screenWidth: Int = displayMetrics.widthPixels
        val screenHeight: Int = displayMetrics.heightPixels

        val screenProportion: Float = screenWidth.toFloat() / screenHeight.toFloat()

        // Get the SurfaceView and the FrameLayout layout parameters
        val slp = surfaceView.layoutParams
        val clp = videoPlayer.layoutParams

        // Set the video player size
        val width: Int
        val height: Int
        if (videoProportion > screenProportion && resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            width = screenWidth
            height = (screenWidth.toFloat() / videoProportion).toInt()
        } else {
            if (videoProportion > (screenProportion / 2) && resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                width = (videoProportion * screenHeight.toFloat()).toInt() / 2
                height = screenHeight /2
            }
            else {
                width = (videoProportion * screenHeight.toFloat()).toInt()
                height = screenHeight
            }
        }
        slp?.width = width
        slp?.height = height
        clp.height = height
    }

    // Set visibility of the media controls
    private fun setVisibility() {
        if (videoControlVisibility) {
            videoControl.visibility = View.GONE //hide textview
            videoControlVisibility = false
        } else {
            videoControl.visibility = View.VISIBLE //show textview
            videoControlVisibility = true
        }
    }

    // Convert millisecond to string.
    private fun millisecondsToString(milliseconds: Int): String {
        val df = DecimalFormat("00")
        val hours = TimeUnit.MILLISECONDS.toHours((milliseconds.toLong()))
        val minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds.toLong())
        val mn = minutes-hours*60
        val seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds.toLong())-(hours+minutes)*60
        if (hours != 0.toLong()) {
            return "$hours:" + df.format(mn) + ":" + df.format(seconds+60)
        }
        return "$mn:" + df.format(seconds)
    }

    private fun start() {
//        if (mediaPlayer != null) {
        currentPosition = mediaPlayer.currentPosition
//        }

        if (currentPosition == duration) {
            // Resets the MediaPlayer to its uninitialized state.
            mediaPlayer.reset()
        }
        mediaPlayer.start()
        // Create a thread to update position of SeekBar.
        val updateSeekBarThread = UpdateSeekBarThread()
        threadHandler.postDelayed(updateSeekBarThread, 50)

        setVisibility()
    }

    private fun pause() {
        this.mediaPlayer.pause()

        val currentPositionStr: String?
        currentPositionStr = millisecondsToString(currentPosition)
        textCurrentPosition.text = currentPositionStr

        seekBar.progress = currentPosition
    }

    // Thread to Update position for SeekBar.
    internal inner class UpdateSeekBarThread : Runnable {

        override fun run() {
//            if (mediaPlayer != null) {
            currentPosition = mediaPlayer.currentPosition
//            }
            val currentPositionStr: String?
            currentPositionStr = millisecondsToString(currentPosition)
            textCurrentPosition.text = currentPositionStr

            seekBar.progress = currentPosition
            // Delay thread 50 milisecond.
            threadHandler.postDelayed(this, 50)
        }
    }

    private fun releaseMediaPlayer() {
        mediaPlayer.release()
        mediaPlayer = MediaPlayer()
    }

    override fun onStop() {
        if(BuildConfig.DEBUG) {
            Log.d("VideoPlayerFragmentGaet", "onStop")
        }
        super.onStop()
        pause()
        isPlaying = mediaPlayer.isPlaying
        releaseMediaPlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseMediaPlayer()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("currentPosition", currentPosition)
        outState.putBoolean("isPlaying", isPlaying)
        super.onSaveInstanceState(outState)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            fullScreen.setImageResource(R.drawable.ic_fullscreen_exit_white)
        }
        else {
            fullScreen.setImageResource(R.drawable.ic_fullscreen_white)
        }

        setVideoSize()
    }
}