package xyz.electricsheep.zeroktube.request.authentication

data class ClientData(val client_id: String, val client_secret: String)