package xyz.electricsheep.zeroktube.request.video

import androidx.room.*

@Dao
interface VideoFilesDao {
    //Get all the files of the database
    @Query("SELECT * FROM files")
    suspend fun getAllFiles() : List<VideoFiles>

    //Get a file based on the videoId it depends
    @Query("SELECT * FROM files WHERE videoId = :videoId")
    suspend fun getAllVideoFiles(videoId : Int) : List<VideoFiles>

    //Delete all the files
    @Query("DELETE FROM files")
    suspend fun deleteAllFiles()

    //Allows you to insert a file into the database, you have to pass every attribut of a file (you also need to give all the attributs of the resolution, the attribute resolution being Embedded it's the equivalent of having every attr of Resolution in the file database
    @Query("INSERT INTO files (fileUrl, fileDownloadUrl, label, videoId) VALUES (:fileUrl, :fileDownloadUrl, :label, :videoId)")
    suspend fun insertFile(fileUrl: String, fileDownloadUrl : String,label  : String, videoId : Int)

    @Update
    suspend fun updateFiles(file : VideoFiles)

    @Delete
    suspend fun deleteFiles(vararg file: VideoFiles)
}