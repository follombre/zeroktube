package xyz.electricsheep.zeroktube.request

interface AbstractSetHostActivity {
    fun onHostSet()
}