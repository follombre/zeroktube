package xyz.electricsheep.zeroktube.request.comments

class DataComments(val total: Int, val data: List<Comment> )