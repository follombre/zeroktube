package xyz.electricsheep.zeroktube.request.authentication

data class UserToken (val access_token: String, val refresh_token: String, val expires_in: Int)