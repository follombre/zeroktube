package xyz.electricsheep.zeroktube.request.video

import androidx.room.*

@Dao public interface ResolutionDao {
    @Query("SELECT * FROM resolution")
    suspend fun getAllResolution() : List<Resolution>

    @Query("DELETE FROM resolution")
    suspend fun deleteAllResolution()

    @Update
    suspend fun updateResolution(resolution : Resolution)

    @Insert
    suspend fun insertAll(vararg resolution: Resolution)

    @Delete
    suspend fun deleteResolution(vararg resolutions : Resolution)
}