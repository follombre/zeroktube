package xyz.electricsheep.zeroktube.request.video

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DescriptionService {
    @GET("api/v1/videos/{uuid}/description")
    fun getDescription(@Path("uuid") uuid:String?): Call<Description>
}