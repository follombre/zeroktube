package xyz.electricsheep.zeroktube.request.video



class VideoListRequest(val total: Int, val data: List<Video> )