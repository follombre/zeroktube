package xyz.electricsheep.zeroktube.request.video

import androidx.room.*

@Dao
interface VideoIdRequestDao{

    //Get all the videos of the database
    @Query("SELECT * FROM videoIdRequest")
    suspend fun getAllVideoIdRequest() : List<VideoIdRequest>

    //Get a video based on its id
    @Query("SELECT * FROM videoIdRequest WHERE id = :idVid")
    suspend fun getVideoIdRequest(idVid : Int) : VideoIdRequest

    //Delete all the files
    @Query("DELETE FROM videoIdRequest")
    suspend fun deleteAllVideoIdRequest()

    //Allows you to insert a video into the database, you have to pass every attribut of a video
    @Query("INSERT INTO videoIdRequest(id,uuid, descriptionPath, name, thumbnailPath, views, likes, dislikes) VALUES (:id, :uuid, :descriptionPath, :name, :thumbnailPath, :views, :likes, :dislikes)")
    suspend fun insertVideo(id: Int, uuid : String?, descriptionPath: String?, name: String?, thumbnailPath: String, views: Int, likes: Int, dislikes: Int)

    @Insert
    suspend fun insertAll(vararg videos: VideoIdRequest)

    @Update
    suspend fun updateVideo(video : VideoIdRequest)

    @Delete
    suspend fun deleteVideoId(vararg videos : VideoIdRequest)

}