package xyz.electricsheep.zeroktube.request.video

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "description")
data class Description (@PrimaryKey(autoGenerate = true) var id: Long,
                        var description: String?,
                        var videoId: Int)
{
    constructor() : this(-1, "", -1)
}