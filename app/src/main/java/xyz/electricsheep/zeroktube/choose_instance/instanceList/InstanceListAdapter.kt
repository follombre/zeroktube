package xyz.electricsheep.zeroktube.choose_instance.instanceList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.choose_instance.instance.Instance

class InstanceListAdapter (private var listInst: List<Instance>, val clickListener: (Instance) -> Unit) :
        RecyclerView.Adapter<InstanceListAdapter.InstanceListViewHolder>() {

    class InstanceListViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        private val insta : TextView = itemView.findViewById(R.id.instName)

        fun bindInstance(inst: Instance, clickListener: (Instance) -> Unit) {
            insta.text = inst.name + " (" + inst.host +")"
            itemView.setOnClickListener { clickListener(inst) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstanceListViewHolder {

        val textView = LayoutInflater.from(parent.context)
                .inflate(R.layout.instancelist_item, parent, false)
        return InstanceListViewHolder(textView)
    }

    /** Replaces the content of a view */
    override fun onBindViewHolder(holder: InstanceListViewHolder, position: Int) {
        holder.bindInstance(listInst[position], clickListener)
    }
    override fun getItemCount() = listInst.size
}


