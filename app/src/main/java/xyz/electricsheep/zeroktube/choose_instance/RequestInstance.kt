package xyz.electricsheep.zeroktube.choose_instance

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.choose_instance.instance.*
import xyz.electricsheep.zeroktube.choose_instance.instanceList.AbstractInstanceListActivity

class RequestInstance{

    private var url = "https://instances.joinpeertube.org/api/v1/"
    private val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()


    fun getInstances(activity: AbstractInstanceListActivity) {
        val service = retrofit.create(ListInstanceService::class.java)
        val instRequest = service.listInstance(0,500, health = true)

        instRequest.enqueue(object : Callback<InstanceListRequest>{
            override fun onResponse(call: Call<InstanceListRequest>, response: Response<InstanceListRequest>) {
                val inst = response.body()
                if(inst != null) activity.onInstanceListLoaded(list = inst.data)
            }

            override fun onFailure(call: Call<InstanceListRequest>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestInstance", "getInstance onFailure")
                }
            }
        })
    }

    fun searchInstance(query : String,activity: AbstractInstanceListActivity){
        val service = retrofit.create(SearchInstanceService::class.java)
        val searchRequest = service.searchInstance(query)

        searchRequest.enqueue(object : Callback<InstanceListRequest>{
            override fun onResponse(call: Call<InstanceListRequest>,response: Response<InstanceListRequest>) {
                val res = response.body()
                if(res != null) activity.onInstanceListLoaded(res.data)
            }

            override fun onFailure(call: Call<InstanceListRequest>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestInstance", "searchInstance onFailure")
                }
            }
        })
    }
}
