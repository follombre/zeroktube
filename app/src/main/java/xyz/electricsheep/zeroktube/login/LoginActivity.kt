package xyz.electricsheep.zeroktube.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.AbstractSetHostActivity
import xyz.electricsheep.zeroktube.request.RequestManager
import xyz.electricsheep.zeroktube.request.authentication.UserToken
import xyz.electricsheep.zeroktube.videolist.AbstractTokenActivity
import xyz.electricsheep.zeroktube.view.VideoListActivity

class LoginActivity : AppCompatActivity(),
        AbstractTokenActivity,
        AbstractSetHostActivity {

    private val requestManager = RequestManager()
    private lateinit var username: EditText
    private lateinit var password: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        username = findViewById(R.id.username)
        password = findViewById(R.id.password)
        requestManager.setHost(this,this.getSharedPreferences("userToken",0).getString("access_token",null)!!)
    }

    override fun onTokenLoaded(token: UserToken) {
        intent = Intent(this,
            VideoListActivity::class.java)
        intent.putExtra("subscriptions",true)
        intent.putExtra("host",getString(R.string.test_instance))
        startActivity(intent)
    }

    fun handleLogin(view: View) {
        val login = username.text.toString()
        val pwd = password.text.toString()
        if(BuildConfig.DEBUG) {
            Log.d("LoginActivityMarius", "handleLogin => $login : $pwd")
        }
        requestManager.fetchUserToken(this,login,pwd)
    }

    override fun onHostSet() {}
}
