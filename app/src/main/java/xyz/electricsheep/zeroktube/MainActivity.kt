package xyz.electricsheep.zeroktube

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import xyz.electricsheep.zeroktube.choose_instance.instanceList.InstanceListActivity
import xyz.electricsheep.zeroktube.login.LoginActivity
import xyz.electricsheep.zeroktube.preferences.PreferencesActivity
import xyz.electricsheep.zeroktube.view.VideoListActivity

class MainActivity : AppCompatActivity() {

    lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val nv: NavController = findNavController(R.id.nav_host_fragment)


        toolbar = supportActionBar!!
        toolbar.title = "ZeroKTube"

        val bottomNavigation: BottomNavigationView = findViewById(R.id.nav_view)
        nv.setGraph(R.navigation.mobile_navigation)
        bottomNavigation.setupWithNavController(nv)

        val videoButton = findViewById<Button>(R.id.video_button)
        videoButton.setOnClickListener {
            startActivity(Intent(this, VideoListActivity::class.java))
        }

        val instButton = findViewById<Button>(R.id.inst_button)
        instButton.setOnClickListener{
            startActivity(Intent(this, InstanceListActivity::class.java))
        }

        val prefButton = findViewById<Button>(R.id.pref_button)
        prefButton.setOnClickListener{
            startActivity(Intent(this, PreferencesActivity::class.java))
        }

        val subscriptionsButton = findViewById<Button>(R.id.sub_button)
        subscriptionsButton.setOnClickListener {
            val intent = Intent(this, VideoListActivity::class.java)
            intent.putExtra("subscriptions",true)
            intent.putExtra("host",getString(R.string.test_instance))
            startActivity(intent)
        }

        val loginButton = findViewById<Button>(R.id.login_button)
        loginButton.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

    }
}
