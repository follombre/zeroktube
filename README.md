<h1 align="center">
    <img src="https://joinpeertube.org/icons/favicon.png" alt="PeerTube">
    ZeroKTube, an Android PeerTube Client
</h1>

<p align=center>
  <strong><a href="https://joinpeertube.org">Official PeerTube's project homepage</a></strong>
  |
  <strong><a href="https://github.com/Chocobozzz/PeerTube">PeerTube's Git</a></strong>
  |
  <strong><a href="https://framasoft.org/">Framasoft's homepage</a></strong>
</p>

<p align=center>
  <a href="https://gitlab.iut-clermont.uca.fr/gabrugiere/zeroktube/-/tags/v0.6.1" alt="Gitlab release - V0.6.1"><img src="https://img.shields.io/badge/Version-v0.6.1%20--%20beta-orange"></a>
  <a href="https://www.gnu.org/licenses/gpl-3.0" alt="License: GPLv3"><img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg"></a>
</p>

## Description
PeerTube is a free, decentralized and federated video platform developed by <a href="https://framasoft.org/">Framasoft</a> as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. But one organization hosting PeerTube alone may not have enough money to pay for bandwidth and video storage of its servers, all servers of PeerTube are interoperable as a federated network, and non-PeerTube servers can be part of the larger Vidiverse (federated video network) by talking the implementation of ActivityPub. Video load is reduced thanks to P2P.

## Screenshots
[<img src="https://gitlab.iut-clermont.uca.fr/gabrugiere/zeroktube/-/raw/master/data/screenshots/VideoMedia.png" alt="Video media player" width=250>](https://gitlab.iut-clermont.uca.fr/gabrugiere/zeroktube/-/blob/master/data/screenshots/VideoMedia.png)

## Features
- [X] Recent Videos
- [X] Choose instance
- [X] Search
- [X] Login
- [X] Video sharing
- [ ] Subscriptions
- [ ] Trending Videos
- [ ] Enable / disable P2P
- [ ] Themes / Dark mode
- [ ] Like/dislike video
- [ ] Video download
- [ ] Comment videos
- [ ] Video quality selection
- [ ] Pull to refresh
- [ ] Background playback
- [ ] NSFW Filter option
- [ ] Create an account
- [ ] Subscribe
- [ ] Report Videos
- [ ] Video speed selection
- [ ] Adding videos
- [ ] Picture-in-picture mode
- [ ] User / Channel Overview Page
- [ ] And more...

## Download
Beta Test coming soon on Google Play and f-Droid.

## Help Translate
Coming soon

## License
Copyright (C) 2019-2020 ZeroKTube developers

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
